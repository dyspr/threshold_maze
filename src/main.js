var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0],
  main: [255, 255, 255]
}

var boardSize

var array = create2DArray(16, 16, 0, false)

var threshold = -0.05
var direction = 0
var layers = 12

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
    layers = Math.floor(12 * (windowHeight / 768))
  } else {
    boardSize = windowWidth - 80
    layers = Math.floor(12 * (windowWidth / 768))
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var k = 0; k < layers; k++) {
    for (var i = 0; i < array.length; i++) {
      for (var j = 0; j < array[i].length; j++) {
        noFill()
        stroke(colors.main[0] * k * (1 / layers), colors.main[1] * k * (1 / layers), colors.main[2] * k * (1 / layers))
        strokeCap(ROUND)
        strokeWeight(boardSize * 0.01)
        if (array[i][j] <= threshold) {
          line(windowWidth * 0.5 + (i - 7) * (42 / 768) * boardSize + k * 2.00 - layers, windowHeight * 0.5 + (j - 7) * (42 / 768) * boardSize + k * 0.25 - layers * 0.25, windowWidth * 0.5 + (i - 8) * (42 / 768) * boardSize + k * 2.00 - layers, windowHeight * 0.5 + (j - 8) * (42 / 768) * boardSize + k * 0.25 - layers * 0.25)
        } else {
          line(windowWidth * 0.5 + (i - 8) * (42 / 768) * boardSize + k * 2.00 - layers, windowHeight * 0.5 + (j - 7) * (42 / 768) * boardSize + k * 0.25 - layers * 0.25, windowWidth * 0.5 + (i - 7) * (42 / 768) * boardSize + k * 2.00 - layers, windowHeight * 0.5 + (j - 8) * (42 / 768) * boardSize + k * 0.25 - layers * 0.25)
        }
      }
    }
  }

  if (direction === 0) {
    threshold += deltaTime * 0.0001
    if (threshold >= 1.05) {
      direction = 1
      array = create2DArray(16, 16, 0, false)
    }
  } else {
    threshold -= deltaTime * 0.0001
    if (threshold <= -0.05) {
      direction = 0
      array = create2DArray(16, 16, 0, false)
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
    layers = Math.floor(12 * (windowHeight / 768))
  } else {
    boardSize = windowWidth - 80
    layers = Math.floor(12 * (windowWidth / 768))
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = Math.random()
      }
    }
    array[i] = columns
  }
  return array
}
